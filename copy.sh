#!/bin/bash

cp ../newsIQ/build/index.html public/.
cp ../newsIQ/build/manifest.json public/.
cp -r ../newsIQ/build/js/* public/js/.
cp -r ../newsIQ/build/css/* public/css/.
cp -r ../newsIQ/build/img/* public/img/.
#cp ../news-curation/src/js/service-worker.js public/.
